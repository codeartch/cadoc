<cfsavecontent variable="request.sidebar"><cfoutput>
	<h3>Documentation</h3><hr />
	<div>
		<a href="javascript:ca.expandAllPackages()" class="package-expand">Expand All</a>
		<a href="javascript:ca.collapseAllPackages()" class="package-collapse">Collapse All</a>
		<div class="clear"></div>
	</div>
	<hr />
	<nav class="package">
		#request.packageNav#
	</nav>
</cfoutput></cfsavecontent>


<cfswitch expression="#request.url.view#">
	<cfcase value="compdetail">
		<cfif Len(request.url.comp)>
			<cfset request.compData		= application.controller.Comp.getData(request.project,request.url.comp)>
		</cfif>
		<cfsavecontent variable="request.content"><cfoutput>
			<cfinclude template="../object/compdetail.cfm">
		</cfoutput></cfsavecontent>
	</cfcase>
	<cfcase value="notfound">
		<cfsavecontent variable="request.content"><cfoutput>
			<cfinclude template="../object/notfound.cfm">
		</cfoutput></cfsavecontent>
	</cfcase>
	<cfdefaultcase>
		<cfsavecontent variable="request.content"><cfoutput>
			<cfinclude template="../object/default/doc.cfm">
		</cfoutput></cfsavecontent>
	</cfdefaultcase>
</cfswitch>