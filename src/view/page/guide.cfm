<cfsavecontent variable="request.sidebar"><cfoutput>
	<h3>Developer Guide</h3><hr />
	<nav class="document">
		#request.documentNav#
	</nav>
</cfoutput></cfsavecontent>


<cfsavecontent variable="request.content">
	<cfinclude template="../object/editalert.cfm">

	<cftry>
		<cfif Len(request.url.view) neq 0>
			<cfinclude template="../../../user_data/content/#request.project.prefix#/#request.url.page#/#request.url.view#.cfm">
		<cfelse>
			<cfinclude template="../object/default/guide.cfm">
		</cfif>
		<cfcatch type="missingInclude">
			<cfinclude template="../object/notfound.cfm">
		</cfcatch>
	</cftry>
</cfsavecontent>
