<!--- <cfinclude template="../object/alert/beta.cfm"> --->
<br />
<div class="page-header">
	<h1>Codeart Documentation <small>Latest changes</small></h1>
</div>

<div class="row-fluid">
	<div class="span4">
		<h4>28.11.2012</h4>
		<h2>Layout update</h2>
		<p>Layout framework updated and some minor layout changes. All platform libraries updated.</p>
		<span class="label label-inverse">Version 1.300</span>
	</div>
	<div class="span4">
		<h4>15.03.2012</h4>
		<h2>Platform update</h2>
		<p>Layout framework updated and some minor layout changes. Accessibility changes. Remote API developer guides added.</p>
		<span class="label label-inverse">Version 1.210</span>
	</div>
	<div class="span4">
		<h4>15.01.2012</h4>
		<h2>Content Update</h2>
		<p>There is now a lot new content in the developer guide section. Please check it out and give us feedback.</p>
		<span class="label label-inverse">Version 1.100</span>
	</div>
	<!--- <div class="span4">
			<h4>01.01.2012</h4>
			<h2>Initial Release</h2>
			<p>The first version of the document center is now online. At the moment there isn't so much content in the developer guide and user manual section. More content coming soon.</p>
			<span class="label label-inverse">Version 1.000</span>
		</div> --->
</div>