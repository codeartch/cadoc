<cfoutput>
	<ul class="nav nav-tabs">
		<li <cfif request.url.tab neq "properties" and request.url.tab neq "methods">class="active"</cfif>>
			<a href="##information" data-toggle="tab">Information</a>
		</li>
		<li <cfif request.url.tab eq "properties">class="active"</cfif>>
			<a href="##properties" data-toggle="tab">Properties</a>
		</li>
		<li <cfif request.url.tab eq "methods">class="active"</cfif>>
			<a href="##methods" data-toggle="tab">Methods</a>
		</li>
	</ul>
	
	<div class="tab-content">
	    <div id="information" class="tab-pane fade <cfif request.url.tab neq 'properties' and request.url.tab neq 'methods'>active in</cfif>">
	    	<div class="breadcrumb">
			    <h4>
			    	<span class="label label-info">#request.compData.metaData.type#</span>
			    	<cfif IsDefined("request.compData.metaData.entityname")>
			    		<span class="label">ORM</span>
			    	</cfif>
			    	#request.compData.comp#
			    </h4>
		    </div>
			<dl class="general">
				<dt>Type</dt>
				<dd>#request.compData.metaData.type#</dd>
				<dt>Package</dt>
				<dd>#request.compData.comp#</dd>
				<dt>Last modified</dt>
				<dd>#request.compData.lastmodified#</dd>
			</dl>
			<dl class="extends">
				<dt>Extends</dt>
				<cfif request.compData.metaData.type eq "interface">
					<dd>#application.controller.Comp.getImplementNav(application.controller.Comp.getImplementInheritance(request.compData.metaData,2),request.project)#</dd>
				<cfelse>
					<dd>#application.controller.Comp.getExtendNav(application.controller.Comp.getExtendInheritance(request.compData.metaData),request.project)#</dd>
				</cfif>
			</dl>
			<dl class="implements">
				<dt>Implements</dt>
				<dd>#application.controller.Comp.getImplementNav(application.controller.Comp.getImplementInheritance(request.compData.metaData),request.project)#</dd>
			</dl>
			<cfif IsDefined("request.compData.metaData.entityname")>
				<dl class="orm">
					<cfif IsDefined("request.compData.metaData.entityname")>
						<dt>Entityname</dt>
						<dd>#request.compData.metaData.entityname#</dd>
					</cfif>
					<cfif IsDefined("request.compData.metaData.persistent")>
						<dt>Persistent</dt>
						<dd>#request.compData.metaData.persistent#</dd>
					</cfif>
					<cfif IsDefined("request.compData.metaData.table")>
						<dt>Tablename</dt>
						<dd>#request.compData.metaData.table#</dd>
					</cfif>
					<cfif IsDefined("request.compData.metaData.mappedsuperclass")>
						<dt>MappedSuperClass</dt>
						<dd>#request.compData.metaData.mappedsuperclass#</dd>
					</cfif>
				</dl>
			</cfif>
			<cfif IsDefined("request.compData.metaData.hint")>
				<dl class="hint">
					<dt>Hint</dt>
					<dd>#request.compData.metaData.hint#</dd>
				</dl>
			</cfif>
			<div class="clear"></div>
		</div>
		
		<div id="properties" class="tab-pane fade <cfif request.url.tab eq 'properties'>active in</cfif>">
			<div class="extends_breadcrumb">
				<div class="title">Extends:</div>
				#application.controller.Comp.getExtendBreadcrumb(application.controller.Comp.getExtendInheritance(request.compData.metaData),request.project,"properties")#
			</div>
			<div class="setting">
				<button id="togglebutton_properties" class="btn" data-toggle="button">Show inherited</button>
			</div>
			<div class="clear"></div>
			<section>
				#application.controller.Comp.getPropertySummary(request.compData.metaData)#
			</section>
		</div>
	
		<div id="methods" class="tab-pane fade <cfif request.url.tab eq 'methods'>active in</cfif>">
			<div class="extends_breadcrumb">
				<div class="title">Extends:</div>
				#application.controller.Comp.getExtendBreadcrumb(application.controller.Comp.getExtendInheritance(request.compData.metaData),request.project,"methods")#
			</div>
			<div class="setting">
				<button id="togglebutton_method" class="btn" data-toggle="button">Show inherited</button>
			</div>
			<div class="clear"></div>
			<section>
				#application.controller.Comp.getMethodSummary(request.compData.metaData)#
			</section>
		</div>
	</div>
</cfoutput>