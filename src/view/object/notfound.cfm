<div class="alert alert-error">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Error</h4>
	<p>Sorry, the page you have requested could not be found.</p>
</div>
