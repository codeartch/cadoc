// Ca functions
ca = new Object();

ca.initContent = function() {
	
	prettyPrint();
	$('#property_tbl').jExpand();
	$('#method_tbl').jExpand();
	$('.content #properties tr.inherited').hide();
	$('.content #methods tr.inherited').hide();
	
	$('#content .page a').each(
		function(column) {
			$(this).click(function(event){
				ca.removeDisable();
				$.ajax({
					url:$(this).attr('href'),
					data:{ajaxrequest:true},
					success:function(response){
						$("#content").html(response);
						ca.initContent();
					}
				});
				event.preventDefault();
			});
		}
	);
	
	$('#togglebutton_method').click(function() {
		if($(this).hasClass("active")) {
			var row = $('.content #methods tr.inherited');
			row.hide();
			row.next('tr').hide();
		} else {
			$('.content #methods tr.inherited').show();
		}
		return;
	});
};


ca.removeDisable = function() {
	$('.sidebar ul a').each(
		function(column) {
			$(this).removeClass('disable');;
		}
	);
};

ca.expandAllPackages = function() {
	$('.sidebar li.node').each(function(){
		$(this).children().show();
		$(this).removeClass('plusimageapply');
		$(this).addClass('minusimageapply');
	});
};

ca.collapseAllPackages = function() {
	$('.sidebar li.node').each(function(){
		$(this).children().hide();
		$(this).removeClass('minusimageapply');
		$(this).addClass('plusimageapply');
	});
};


// On document ready
$(function() {

	ca.initContent();
	
	/*$('.main-wrap .sidebar').height($(window).height()-$('.top-wrap').height()-$('.bot-wrap').height()-40);
	$(window).resize(function(){
	   $('.main-wrap .sidebar').height($(window).height()-$('.top-wrap').height()-$('.bot-wrap').height()-40);
	});*/
		
	//Tree Expand / Collapse
	$('.sidebar li.node').addClass('plusimageapply');
	$('.sidebar li.node').children().addClass('selectedimage');
	$('.sidebar li.node').children().hide();
	$('.sidebar li.node').each(
		function(column) {
			$(this).click(function(event){
				if (this == event.target) {
					if($(this).is('.plusimageapply')) {
						$(this).children().show();
						$(this).removeClass('plusimageapply');
						$(this).addClass('minusimageapply');
					} else {
						$(this).children().hide();
						$(this).removeClass('minusimageapply');
						$(this).addClass('plusimageapply');
					}
				}
			});
		}
	);
	
	$('.sidebar ul a').each(
		function(column) {
			$(this).click(function(event){
				ca.removeDisable();
				$(this).addClass('disable');
				
				$.ajax({
					url:$(this).attr('href'),
					data:{ajaxrequest:true},
					success:function(response){
						$("#content").html(response);
						ca.initContent();
					}
				});
				event.preventDefault();
			});
		}
	);

});