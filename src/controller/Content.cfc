<cfcomponent extends="_global" hint="Content Object">
	
		
	<cffunction name="init" access="public" returntype="struct" output="false" hint="Constructor">
		<cfargument name="path" required="true" type="string" hint="Path to the data.json file">
		
		<cfset variables.instance 			= StructNew()>
		<cfset variables.instance.path	 	= arguments.path>
		<cfset variables.instance.content 	= readJsonData()>
		
		<cfreturn this />
	</cffunction>
	
	
	<cffunction name="reloadJsonData" access="public" returntype="void" output="false" hint="Read and deserialize the json content file.">
		
		<cfset variables.instance.content 	= readJsonData()>
			 
	</cffunction>
	
	
	<cffunction name="readJsonData" access="public" returntype="array" output="false" hint="Read and deserialize the json content file. Returns the Array">
		
		<cfset var returnData = ArrayNew(1)>
		
		<cftry>
			<cfset returnData	= DeserializeJson(FileRead(variables.instance.path))>
			<cfcatch type="any"></cfcatch>
		</cftry>
			 
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="saveJsonData" access="public" returntype="void" output="false" hint="Serializes and saves the data to the json content file">
		
		<cfset FileWrite(variables.instance.path,SerializeJson(variables.instance.content))>
		
	</cffunction>


</cfcomponent>