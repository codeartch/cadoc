<cfcomponent extends="_global" hint="Filesystem Object">
	
	
	<cffunction name="listByProject" access="public" returntype="query" output="false" hint="The list method returns a list of all files in the directory path that match the given filter.">
		<cfargument name="project" required="true" type="struct" hint="Project Config Struct">
		
		<cfset var listData = QueryNew("")>
		
		<cfdirectory name="listData" action="list" directory="#arguments.project.path#" recurse="#arguments.project.recurse#" filter="#arguments.project.filter#"> 
		
		<cfreturn listData />
	</cffunction>
	
	
	<cffunction name="listByPath" access="public" returntype="query" output="false" hint="The list method returns a list of all files in the directory path.">
		<cfargument name="path" required="true" type="string" hint="Project Config Struct">
		
		<cfset var listData = QueryNew("")>
		
		<cfdirectory name="listData" action="list" directory="#arguments.path#" recurse="true" filter="*.cfm"> 
		
		<cfreturn listData />
	</cffunction>


</cfcomponent>