<cfcomponent extends="_global" hint="Componenten Object">
	
	
	<cffunction name="init" access="public" returntype="struct" output="false" hint="Constructor">
		<cfargument name="listStorePackage" required="true" type="struct" hint="List Store">
		
		<cfset variables.instance 					= StructNew()>
		<cfset variables.instance.listStorePackage	= arguments.listStorePackage>
		
		<cfreturn this />
	</cffunction>	


	<cffunction name="getData" access="public" returntype="struct" output="false" hint="Returns a data struct for the given component">
		<cfargument name="project" required="true" type="struct" hint="Project Config Struct">
		<cfargument name="comp" required="true" type="string" hint="Comp path">
		
		<cfset var returnData	= StructNew()>
		<cfset var queryFile	= "">
		<cfset var callFile		= "">
		<cfset var tmpDate		= "">
		<cfset var projectFile	= variables.instance.listStorePackage[arguments.project.prefix]>
		
		<cfset returnData.comp 		= arguments.comp>
		<cfset returnData.project 	= arguments.project>
		<cfset returnData.metaData	= GetComponentMetaData("doc_#arguments.project.prefix#.#arguments.comp#")>
		
		<cfloop query="projectFile">
			<cfset queryFile	= "#projectFile.directory#/#projectFile.name#">
			<cfset callFile		= returnData.metaData.path>
			
			<cfif queryFile eq callFile>
				<cfset tmpDate = projectFile.datelastmodified>
				<cfbreak>
			</cfif>
		</cfloop>
		<cfset returnData.lastmodified 	= tmpDate>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getExtendBreadcrumb" access="public" returntype="string" output="false" hint="Returns the HTML code for the extends breadcrumb">
		<cfargument name="inheritance" required="true" type="array" hint="Inheritance Array (from getExtendInheritance)">
		<cfargument name="project" required="true" type="struct" hint="Project Config Struct">
		<cfargument name="type" required="false" type="string" default="" hint="tab type">
		
		<cfset var returnData = "">
		<cfset var item = StructNew()>
		<cfset var cls = "">
		<cfset var tmpComp = "">
		<cfset var tab = "">
		
		<cfset ArrayDeleteAt(arguments.inheritance,ArrayLen(arguments.inheritance))>
		
		<cfif ArrayLen(arguments.inheritance) gte 1>
			<cfsavecontent variable="returnData"><cfoutput>
				<ul>
					<cfloop array="#arguments.inheritance#" index="item">
						<cfset cls = "page">
						<cfset tab = "">
						<cfset tmpComp = getCompPath(item.path)>
						
						<cfif arguments.project.globalfile eq ListLast(tmpComp,".") & ".cfc">
							<cfset cls = cls & " global">
						</cfif>
						
						<cfif Len(arguments.type) gt 2>
							<cfset tab = "&amp;tab=#arguments.type#">
						</cfif>
						
						<li class="#cls#">
							<a href="?project=#arguments.project.prefix#&amp;page=doc&amp;view=compdetail&amp;comp=#URLEncodedFormat(tmpComp)##tab#">#tmpComp#</a>
						</li>
					</cfloop>
				</ul>
			</cfoutput></cfsavecontent>
		<cfelse>
			<cfsavecontent variable="returnData"><cfoutput>
				<ul>
					<li class="empty">-</li>
				</ul>
			</cfoutput></cfsavecontent>
		</cfif>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getExtendNav" access="public" returntype="string" output="false" hint="Returns the HTML code for the extends navigation">
		<cfargument name="inheritance" required="true" type="array" hint="Inheritance Array (from getExtendInheritance)">
		<cfargument name="project" required="true" type="struct" hint="Project Config Struct">
		<cfargument name="level" required="false" type="numeric" default="1" hint="(Private) Recursive level">
		
		<cfset var returnData = "">
		<cfset var item = StructNew()>
		<cfset var cls = "">
		<cfset var tmpComp = "">
		
		<cfif (ArrayLen(arguments.inheritance) gte 2 and arguments.level eq 1) or (ArrayLen(arguments.inheritance) gte 1 and arguments.level gt 1)>
		
			<cfset cls = "page">
			<cfset tmpComp = getCompPath(arguments.inheritance[1].path)>
			
			<cfif arguments.project.globalfile eq ListLast(tmpComp,".") & ".cfc">
				<cfset cls = cls & " global">
			</cfif>
		
			<cfsavecontent variable="returnData"><cfoutput>
				<ul>
					<li class="#cls#">
						<a href="?project=#arguments.project.prefix#&amp;page=doc&amp;view=compdetail&amp;comp=#URLEncodedFormat(tmpComp)#">#tmpComp#</a>
					</li>
					<cfset ArrayDeleteAt(arguments.inheritance,1)>
					<cfif ArrayLen(arguments.inheritance) gte 1>
						#getExtendNav(arguments.inheritance,arguments.project,arguments.level+1)#
					</cfif>
				</ul>
			</cfoutput></cfsavecontent>
		<cfelse>
			<cfset var returnData = "-">
		</cfif>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getImplementNav" access="public" returntype="string" output="false" hint="Returns the HTML code for the implements navigation">
		<cfargument name="inheritance" required="true" type="struct" hint="Inheritance Struct (from getImplementInheritance)">
		<cfargument name="project" required="true" type="struct" hint="Project Config Struct">
		<cfargument name="level" required="true" type="numeric" default="1" hint="(Private) Recursive level">
		
		<cfset var returnData = "">
		<cfset var item = StructNew()>
		<cfset var cls = "">
		<cfset var tmpComp = "">
		
		<cfif IsDefined("arguments.inheritance.items")>
			<cfsavecontent variable="returnData"><cfoutput>
				<cfloop array="#arguments.inheritance.items#" index="item">
					<cfset cls = "page">
					<cfset tmpComp = getCompPath(item.meta.path)>
					
					<cfif arguments.project.globalfile eq ListLast(tmpComp,".") & ".cfc">
						<cfset cls = cls & " global">
					</cfif>
					
					<ul>
						<li class="#cls#">
							<a href="?project=#arguments.project.prefix#&amp;page=doc&amp;view=compdetail&amp;comp=#URLEncodedFormat(tmpComp)#">#tmpComp#</a>
						</li>
						<cfif StructKeyExists(item,"items") and ArrayLen(item.items) gte 1>
							#getImplementNav(item,arguments.project,arguments.level+1)#
						</cfif>
					</ul>
				</cfloop>
			</cfoutput></cfsavecontent>
		</cfif>
		
		<cfif arguments.level eq 1 and Len(returnData) lte 2>
			<cfset var returnData = "-">
		</cfif>
		
		<cfreturn returnData />
	</cffunction>
		
	
	<cffunction name="getExtendInheritance" access="public" returntype="array" output="false" hint="Returns the componente extend inheritance array">
		<cfargument name="metaData" required="true" type="struct" hint="Comp metaData">
		
		<cfset var returnData = ArrayNew(1)>
		
		<cfif StructKeyExists(arguments.metaData,"extends") and StructKeyExists(arguments.metaData.extends,"name") and not FindNoCase("WEB-INF",arguments.metaData.extends.name)>
			<cfset returnData.addAll(getExtendInheritance(arguments.metaData.extends))>
		</cfif>
		
		<cfset ArrayAppend(returnData,arguments.metaData)>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getImplementInheritance" access="public" returntype="struct" output="false" hint="Returns the componente implement inheritance array">
		<cfargument name="metaData" required="true" type="struct" hint="Comp metaData">
		<cfargument name="level" required="false" type="numeric" default="1" hint="(Private) Recursive level">

		<cfset var returnData = StructNew()>
		<cfset var tmpData = Structnew()>
		<cfset var item = StructNew()>
		<cfset var mode = "implements">
		
		<cfif arguments.level gt 1>
			<cfset mode = "extends">
		</cfif>
		
		<cfif StructKeyExists(arguments.metaData,mode)>
			<cfset returnData.items = ArrayNew(1)>
			<cfset returnData.meta = arguments.metaData>
			<cfloop collection="#arguments.metaData[mode]#" item="item">
				<cfif not FindNoCase("WEB-INF",arguments.metaData[mode][item].name)>
					<cfset ArrayAppend(returnData.items,getImplementInheritance(arguments.metaData[mode][item],arguments.level+1))>
				</cfif>
			</cfloop>
		</cfif>
		
		<cfif arguments.level eq 1>
			<cfset returnData.meta.items = "">
		</cfif>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getPropertySummary" access="public" returntype="string" output="false" hint="Returns the HTML code for the propery summary">
		<cfargument name="metaData" required="true" type="struct" hint="Comp metaData">
		
		<cfset var returnData = "">
		<cfset var item = StructNew()>
		<cfset var item_p = StructNew()>
		
		<cfset var methodeInheritance = application.controller.Comp.getInheritance(request.compData.metaData,"properties")>
   		<cfset var methodData = application.controller.Comp.createMethodData(methodeInheritance)>
		
		<cfsavecontent variable="returnData"><cfoutput>
			<table class="table table-bordered table-striped" id="property_tbl">
				<thead>
					<tr>
						<th class="icon"></th>
						<th class="type">Type</th>
						<th class="name">Name</th>
						<th class="hint">Hint</th>
						<th class="flags">Flags</th>
					</tr>
				</thead>
				<tbody>
					<cfloop array="#methodData#" index="item">
						<cfif item.inheritance[1].level eq 1>
							<tr class="self">
						<cfelse> 
							<tr class="inherited">
						</cfif>
							<td><div class="open"></div></td>
							<td>
								<cfif IsDefined("item.type")>
									<span class="label label-info">#item.type#</span>
								<cfelse>
									<span class="label label-info">any</span>
								</cfif>
							</td>
							<td>
								<span class="name">#item.name#</span>
							</td>
							<td>
								<cfif IsDefined("item.hint")><code class="hint">#item.hint#</code></cfif>
							</td>
							<td>
								<cfif IsDefined("arguments.metaData.entityname")>
									<span class="label">orm</span>
								</cfif>
								<cfif item.inheritance[1].level eq 1>
									<cfif ArrayLen(item.inheritance) gt 1>
										<span class="label label-important">extended</span>
									</cfif>
								<cfelse>
									<span class="label label-important">inherited</span>
								</cfif>
							</td>
						</tr>
						<tr>
							<td colspan="5" class="detail">
								<h5>Detail</h5>
								<ul>
								<cfif IsDefined("item.fieldtype")>
									<li class="fieldtype">Fieldtype: #item.fieldtype#</li>
								</cfif>
								<cfif IsDefined("item.ormtype")>
									<li class="ormtype">Ormtype: #item.ormtype#</li>
								</cfif>
								<cfif IsDefined("item.generator")>
									<li class="generator">Generator: #item.generator#</li>
								</cfif>
								<cfif IsDefined("item.getter")>
									<li class="getter">Getter: #item.setter#</li>
								</cfif>
								<cfif IsDefined("item.setter")>
									<li class="setter">Setter: #item.setter#</li>
								</cfif>
								</ul>
							</td>
						</tr>
					</cfloop>
				</tbody>
			</table>
		</cfoutput></cfsavecontent>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getMethodSummary" access="public" returntype="string" output="false" hint="Returns the HTML code for the method summary">
		<cfargument name="metaData" required="true" type="struct" hint="Comp metaData">
		
		<cfset var returnData = "">
		<cfset var item = StructNew()>
		<cfset var item_p = StructNew()>
		
		<cfset var methodeInheritance = application.controller.Comp.getInheritance(request.compData.metaData,"functions")>
   		<cfset var methodData = application.controller.Comp.createMethodData(methodeInheritance)>
		
		<cfsavecontent variable="returnData"><cfoutput>
			<table class="table table-bordered table-striped" id="method_tbl">
				<thead>
					<tr>
						<th class="icon"></th>
						<th class="access">Access</th>
						<th class="name">Name</th>
						<th class="call"></th>
						<th class="return">Returns</th>
						<th class="flags">Flags</th>
					</tr>
				</thead>
				<tbody>
					<cfloop array="#methodData#" index="item">
						<cfif item.inheritance[1].level eq 1>
							<tr class="self">
						<cfelse> 
							<tr class="inherited">
						</cfif>
							<td><div class="open"></div></td>
							<td>
								<cfif IsDefined("item.access")>
									<span class="label label-info">#item.access#</span>
								<cfelse>
									<span class="label label-info">public</span>
								</cfif>
							</td>
							<td>
								<span class="name">#item.name#</span>
							</td>
							<td>
								<code>#createMethodCall(item.name,item.parameters)#</code>
							</td>
							<td>
								<cfif IsDefined("item.returntype")>
									<span class="returntype label">#item.returntype#</span>
								<cfelse>
									<span class="returntype label">any</span>
								</cfif>
							</td>
							<td>
								<cfif item.inheritance[1].level eq 1>
									<cfif ArrayLen(item.inheritance) gt 1>
										<span class="label label-important">extended</span>
									</cfif>
								<cfelse>
									<span class="label label-important">inherited</span>
								</cfif>
								<cfif item.sorttype eq "const">
									<span class="label label-warning">constructor</span>
								</cfif>
								<cfif item.sorttype eq "ready">
									<span class="label label-success">onready</span>
								</cfif>
							</td>
						</tr>
						<tr>
							<td colspan="6" class="detail">
								<cfif IsDefined("item.hint") or IsDefined("item.roles")>
									<h5>Detail</h5>
									<cfif IsDefined("item.hint")>
										<span class="hint">#item.hint#</span>
									</cfif>
									<cfif IsDefined("item.roles")>
										<span class="roles">#item.roles#</span>
									</cfif>
								</cfif>
								<cfif ArrayLen(item.parameters) gte 1>
									<h5>Parameters</h5>
								</cfif>
								<cfloop array="#item.parameters#" index="item_p">
									<section class="param">
										<div class="row show-grid">
									    	<div class="span3">
									    		<cfif IsDefined("item_p.type")>
													<span class="type label">#item_p.type#</span>
												<cfelse>
													<span class="type label">any</span>
												</cfif>
									    		<span class="name">#item_p.name#</span>
									    	</div>
									    	<cfif IsDefined("item_p.hint")>
									    		<div class="span8">#item_p.hint#</div>
									    	</cfif>
									  	</div>
									</section>
								</cfloop>
								<h5>Returns</h5>
								<cfif IsDefined("item.returntype")>
									<span class="returntype"><span class="label">#item.returntype#</span></span>
								<cfelse>
									<span class="returntype"><span class="label">any</span></span>
								</cfif>
							</td>
						</tr>
					</cfloop>
				</tbody>
			</table>
		</cfoutput></cfsavecontent>
		
		<cfreturn returnData />
	</cffunction>


	<cffunction name="getInheritance" access="public" returntype="array" output="false" hint="Returns the inheritance array">
		<cfargument name="metaData" required="true" type="struct" hint="Comp metaData">
		<cfargument name="type" required="true" type="string" hint="functions or properties">
		<cfargument name="level" required="false" type="numeric" default="1" hint="(Private) Recursive level">
		<cfargument name="constructor" required="false" type="string" default="#request.project.constructor#" hint="Constructor list">
		<cfargument name="onready" required="false" type="string" default="#request.project.onready#" hint="OnReady list">
		
		<cfset var returnData = ArrayNew(1)>
		<cfset var item = StructNew()>
		<cfset var children = StructNew()>
		
		<cfif StructKeyExists(arguments.metaData,"extends") and StructKeyExists(arguments.metaData.extends,"name") and not FindNoCase("WEB-INF",arguments.metaData.extends.name)>
			<cfset returnData = getInheritance(arguments.metaData.extends,arguments.type,arguments.level+1)>	
		</cfif>
		
		<cfif StructKeyExists(arguments.metaData,arguments.type)>
			<cfloop array="#arguments.metaData[arguments.type]#" index="item">
				<cfif ListFind(arguments.constructor,item.name)>
					<cfset item.sorttype = "const">
				<cfelseif ListFind(arguments.onready,item.name)>
					<cfset item.sorttype = "ready">
				<cfelse>
					<cfset item.sorttype = "abc">
				</cfif>
				
				<cfset item.inheritance = StructNew()>
				<cfset item.inheritance.level = arguments.level>
				<cfset item.inheritance.comp = getCompName(arguments.metaData.name)>
				<cfif item.inheritance.level gt 1>
					<cfset item.inheritance.inherit = true>
				<cfelse>
					<cfset item.inheritance.inherit = false>
				</cfif>
				<cfset ArrayAppend(returnData,item)>
			</cfloop>
		</cfif>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="createMethodData" access="public" returntype="array" output="false" hint="Returns the method data for the given methode array">
		<cfargument name="data" required="true" type="array" hint="Method data (from getInheritance)">
		<cfargument name="hidePrivateMethods" required="false" type="string" default="#request.project.hidePrivateMethods#" hint="hidePrivateMethods setting">
		<cfargument name="stickOnTop" required="false" type="boolean" default="#application.config.stickOnTop#" hint="StickOnTop">
		
		<cfset var returnData = ArrayNew(1)>
		<cfset var constData = ArrayNew(1)>
		<cfset var readyData = ArrayNew(1)>
		<cfset var abcData = ArrayNew(1)>
		<cfset var sortData = ArrayNew(1)>
		
		<cfset var constHelper = "">
		<cfset var readyHelper = "">
		<cfset var abcHelper = "">
		<cfset var sortHelper = "">
		
		<cfset var item = StructNew()>
		<cfset var inheritance = ArrayNew(1)>
		
		<cfset sortData = ArrayOfStructSort(arguments.data,"textnocase","asc","name")>
		
		<cfloop array="#sortData#" index="item">
			<cfif (arguments.hidePrivateMethods and IsDefined("item.access") and item.access neq "private") or (arguments.hidePrivateMethods and not IsDefined("item.access")) or not arguments.hidePrivateMethods>
				<cfif not ListFind(sortHelper,item.name)>
					
					<cfset inheritance = ArrayNew(1)>
					<cfset ArrayAppend(inheritance,item.inheritance)>
					<cfset item.inheritance = inheritance>
					
					<cfif arguments.stickOnTop>
						<cfif item.sorttype eq "const">
							<cfset ArrayAppend(constData,item)>
							<cfset constHelper = ListAppend(constHelper,item.name)>
						<cfelseif  item.sorttype eq "ready">
							<cfset ArrayAppend(readyData,item)>
							<cfset readyHelper = ListAppend(readyHelper,item.name)>
						<cfelse>
							<cfset ArrayAppend(abcData,item)>
							<cfset abcHelper = ListAppend(abcHelper,item.name)>
						</cfif>
					<cfelse>
						<cfset ArrayAppend(abcData,item)>
						<cfset abcHelper = ListAppend(abcHelper,item.name)>
					</cfif>
					
					<cfset sortHelper = ListAppend(sortHelper,item.name)>
				<cfelse>
					<cfif arguments.stickOnTop>
						<cfif item.sorttype eq "const">
							<cfset ArrayAppend(constData[ListFind(constHelper,item.name)].inheritance,local.item.inheritance)>
						<cfelseif item.sorttype eq "ready">
							<cfset ArrayAppend(readyData[ListFind(readyHelper,item.name)].inheritance,local.item.inheritance)>
						<cfelse>
							<cfset ArrayAppend(abcData[ListFind(abcHelper,item.name)].inheritance,local.item.inheritance)>
						</cfif>
					<cfelse>
						<cfset ArrayAppend(abcData[ListFind(abcHelper,item.name)].inheritance,local.item.inheritance)>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		
		<cfset returnData.addAll(constData)>
		<cfset returnData.addAll(readyData)>
		<cfset returnData.addAll(abcData)>
		
		<cfloop array="#returnData#" index="item">
			<cfset item.inheritance = ArrayOfStructSort(item.inheritance ,"numeric","asc","level")>
		</cfloop>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="createMethodCall" access="private" returntype="string" output="false" hint="Returns method call string">
		<cfargument name="methodname" required="true" type="string" hint="method name">
		<cfargument name="parameters" required="true" type="array" hint="method parameters">
		
		<cfset var returnData = "">
		<cfset var item = StructNew()>
		<cfset var required = false>
		
		<cfsavecontent variable="returnData"><cfoutput>
			 <span class="methodeopen">#arguments.methodname#(</span>
			<cfloop array="#arguments.parameters#" index="item">
				<cfif IsDefined("item.required")>
					<cfset required = item.required>
				</cfif>
				<cfif not required>
					<span class="brakedopen">[</span>
				</cfif>
				<cfif IsDefined("item.type")>
					<span class="type">#item.type#</span>
				<cfelse>
					<span class="type">any</span>
				</cfif>
				<span class="name">#item.name#</span>
				<cfif not required>
					<span class="brakedclose">]</span>
				</cfif>
				<cfif arguments.parameters[ArrayLen(arguments.parameters)].name neq item.name>,</cfif>
			</cfloop>
			<span class="methodeclose">)</span>
		</cfoutput></cfsavecontent>
		
		<cfreturn returnData />
	</cffunction>


</cfcomponent>