<cfcomponent extends="_global" hint="Util Object">
	
	<cffunction name="init" access="public" returntype="struct" output="false" hint="Constructor">
		<cfargument name="listStorePackage" required="true" type="struct" hint="List Store">
		<cfargument name="listStorePage" 	required="true" type="struct" hint="List Store">
		
		<cfset variables.instance 					= StructNew()>
		<cfset variables.instance.listStorePackage	= arguments.listStorePackage>
		<cfset variables.instance.listStorePage		= arguments.listStorePage>
		
		<cfreturn this />
	</cffunction>
	

	<cffunction name="formatCode" access="public" returntype="string">
		<cfargument name="code" required="true" type="string">
		
		<cfset arguments.code = Trim(arguments.code)>
		<cfset arguments.code = ReplaceNoCase(arguments.code,"<","&lt;","all")>
		<cfset arguments.code = ReplaceNoCase(arguments.code,">","&gt;","all")>
		
		<cfreturn arguments.code>
	</cffunction>
	
	
	<cffunction name="getProjectNav" access="public" returntype="string" output="false" hint="">
		<cfargument name="project" required="true" type="array" hint="Struct with all projects">
		<cfargument name="currentProjectPrefix" required="true" type="string" hint="Current project prefix">
		<cfargument name="cls" required="false" type="string" default="nav" hint="Ul css class">
		
		<cfset var item = "">
		
		<cfsavecontent variable="returnData"><cfoutput>
			<ul class="#arguments.cls#">
				<cfloop array="#arguments.project#" index="item">
					<cfif item.module.doc or item.module.guide or item.module.manual or item.module.download or item.module.demo>
						<li class="dropdown <cfif item.prefix eq arguments.currentProjectPrefix>active</cfif>">
							<a class="dropdown-toggle" data-toggle="dropdown" href="##">#item.name# <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<cfif item.module.doc>
									<cfif Len(Trim(item.module.altdoc)) eq 0>
										<li><a href="?project=#item.prefix#&amp;page=doc">Documentation</a></li>
									<cfelse>
										<li><a href="#item.module.altdoc#" target="_blank">Documentation</a></li>
									</cfif>
								</cfif>
								<cfif item.module.guide>
									<cfif Len(Trim(item.module.altguide)) eq 0>
										<li><a href="?project=#item.prefix#&amp;page=guide">Developer Guides</a></li>
									<cfelse>
										<li><a href="#item.module.altguide#" target="_blank">Developer Guides</a></li>
									</cfif>
								</cfif>
								<cfif item.module.manual>
									<cfif Len(Trim(item.module.altmanual)) eq 0>
										<li><a href="?project=#item.prefix#&amp;page=manual">User Manual</a></li>
									<cfelse>
										<li><a href="#item.module.altmanual#" target="_blank">User Manual</a></li>
									</cfif>
								</cfif>
								<cfif (item.module.doc or item.module.guide or item.module.manual) and (item.module.download or item.module.demo)>
									<li class="divider"></li>
								</cfif>
								<cfif item.module.download>
									<cfif Len(Trim(item.module.altdownload)) eq 0>
										<li><a href="?project=#item.prefix#&amp;page=download">Download</a></li>
									<cfelse>
										<li><a href="#item.module.altdownload#" target="_blank">Download</a></li>
									</cfif>
								</cfif>
								<cfif item.module.demo>
									<cfif Len(Trim(item.module.altdemo)) eq 0>
										<li><a href="?project=#item.prefix#&amp;page=demo">Demo</a></li>
									<cfelse>
										<li><a href="#item.module.altdemo#" target="_blank">Demo</a></li>
									</cfif>
								</cfif>
							</ul>
						</li>
					</cfif>
				</cfloop>
			</ul>
		</cfoutput></cfsavecontent>

		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getPackageStruct" access="public" returntype="struct" output="false" hint="">
		<cfargument name="project" required="true" type="struct" hint="project config">
		<cfargument name="delimiter" required="false" type="string" default="#application.config.delimiter#">
		
		<cfscript>
			var returnData 			= StructNew();
			returnData.name			= arguments.project.prefix;
			returnData.type			= "node";
			returnData.children		= getPackageStructLevel(variables.instance.listStorePackage[arguments.project.prefix],arguments.project,arguments.project.path,arguments.delimiter);
		</cfscript>

		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getPackageStructLevel" access="public" returntype="array" output="false" hint="">
		<cfargument name="dirQuery" required="true" type="query" hint="query from cfdirectory">
		<cfargument name="project" required="true" type="struct" hint="project config">
		<cfargument name="path" required="true" type="string" hint="Path">
		<cfargument name="delimiter" required="true" type="string">
		
		<cfset var qryElemenets 	= QueryNew("")>
		<cfset var qryChildren	 	= QueryNew("")>
		<cfset var dirChildren	 	= "">
		<cfset var returnData		= ArrayNew(1)>

		<cfquery name="qryElemenets" dbtype="query">
			SELECT *
			FROM arguments.dirQuery
			WHERE directory = '#arguments.path#'
			ORDER BY directory 
		</cfquery>
		
		<cfquery name="qryChildren" dbtype="query">
			SELECT *
			FROM arguments.dirQuery
			WHERE directory like '%#arguments.path##arguments.delimiter#%'
			ORDER BY directory 
		</cfquery>
		
		<cfset var tmp	= "">
		<cfloop query="qryChildren">
			<cfset tmp = ListFirst(Replace(qryChildren.directory,arguments.path,""),arguments.delimiter)>
			<cfif not ListContains(dirChildren,tmp)>
				<cfset dirChildren = ListAppend(dirChildren,tmp)>
			</cfif>
		</cfloop>
		
		<cfset var item	= "">
		<cfloop list="#dirChildren#" index="item">
			<cfscript>
				var tmpData 			= StructNew();
				tmpData.name			= item;
				tmpData.type			= "node";
				tmpData.cls				= "";
				tmpData.prefix			= arguments.project.prefix;
				tmpData.children		= getPackageStructLevel(arguments.dirQuery,arguments.project,"#arguments.path##arguments.delimiter##item#",arguments.delimiter);
				ArrayAppend(returnData,tmpData);
			</cfscript>			
		</cfloop>
		
		<cfloop query="qryElemenets">
			<cfscript>
				var tmpData 			= StructNew();
				tmpData.name			= ListFirst(qryElemenets.name,".");
				tmpData.type			= "page";
				tmpData.cls				= "#ListLast(qryElemenets.name,'.')#";
				tmpData.prefix			= arguments.project.prefix;
				tmpData.comp			= getCompPath(qryElemenets.directory & #arguments.delimiter# & qryElemenets.name,arguments.project.path);
				
				if(qryElemenets.name eq arguments.project.globalfile) {
					tmpData.cls = tmpData.cls & " global";
				}
				
				ArrayAppend(returnData,tmpData);
			</cfscript>			
		</cfloop>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getPackageNav" acc ess="public" returntype="string" output="false" hint="returns the navigation struct as html">
		<cfargument name="navStruct" required="true" type="struct" hint="nav struct">
		
		<cfset var returnData		= "">
		
		<cfsavecontent variable="returnData"><cfoutput>
			<!--- <ul>
				<li>
					#arguments.navStruct.name#
					#getPackageNavLevel(arguments.navStruct.children)#
				</li>
			</ul> --->
			#getPackageNavLevel(arguments.navStruct.children)#
		</cfoutput></cfsavecontent>

		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getPackageNavLevel" access="public" returntype="string" output="false" hint="returns the navigation children struct as html">
		<cfargument name="children" required="true" type="array" hint="nav struct">
		
		<cfset var index			= 0>
		<cfset var returnData		= "">
		
		<cfsavecontent variable="returnData"><cfoutput>
			<ul>
				<cfloop from="1" to="#ArrayLen(arguments.children)#" index="index">
					<li class="#arguments.children[index].type# #arguments.children[index].cls#">
						<cfif arguments.children[index].type eq "node">
							#arguments.children[index].name#
							#getPackageNavLevel(arguments.children[index].children)#
						<cfelse>
							<a href="?project=#arguments.children[index].prefix#&amp;page=doc&amp;view=compdetail&amp;comp=#URLEncodedFormat(arguments.children[index].comp)#">#arguments.children[index].name#</a>	
						</cfif>
					</li>
				</cfloop>
				<cfif ArrayLen(arguments.children) eq 0>
					<li class="empty">No content</li>
				</cfif>
			</ul>
		</cfoutput></cfsavecontent>

		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getDocStruct" access="public" returntype="struct" output="false" hint="">
		<cfargument name="project" required="true" type="struct" hint="project config">
		<cfargument name="page" required="true" type="struct" hint="page">
		<cfargument name="delimiter" required="false" type="string" default="#application.config.delimiter#">
		
		<cfscript>
			var returnData 			= StructNew();
			returnData.name			= arguments.page.key;
			returnData.type			= "node";
			returnData.children		= getDocStructLevel(variables.instance.listStorePage[arguments.project.prefix][arguments.page.key],arguments.project,Replace(arguments.page.path,"{projectprefix}",arguments.project.prefix),arguments.page,arguments.delimiter);
		</cfscript>

		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getDocStructLevel" access="public" returntype="array" output="false" hint="">
		<cfargument name="dirQuery" required="true" type="query" hint="query from cfdirectory">
		<cfargument name="project" required="true" type="struct" hint="project config">
		<cfargument name="path" required="true" type="string" hint="Path">
		<cfargument name="page" required="true" type="struct" hint="page">
		<cfargument name="delimiter" required="true" type="string">
		
		<cfset var qryElemenets 	= QueryNew("")>
		<cfset var qryChildren	 	= QueryNew("")>
		<cfset var dirChildren	 	= "">
		<cfset var returnData		= ArrayNew(1)>

		<cfquery name="qryElemenets" dbtype="query">
			SELECT *
			FROM arguments.dirQuery
			WHERE directory = '#arguments.path#'
			ORDER BY directory 
		</cfquery>
		
		<cfquery name="qryChildren" dbtype="query">
			SELECT *
			FROM arguments.dirQuery
			WHERE directory like '%#arguments.path##arguments.delimiter#%'
			ORDER BY directory 
		</cfquery>
		
		<cfset var tmp	= "">
		<cfloop query="qryChildren">
			<cfset tmp = ListFirst(Replace(qryChildren.directory,arguments.path,""),arguments.delimiter)>
			<cfif not ListContains(dirChildren,tmp)>
				<cfset dirChildren = ListAppend(dirChildren,tmp)>
			</cfif>
		</cfloop>
		
		<cfset var item	= "">
		<cfloop list="#dirChildren#" index="item">
			<cfscript>
				var tmpData 			= StructNew();
				tmpData.name			= item;
				tmpData.type			= "node";
				tmpData.cls				= "";
				tmpData.prefix			= arguments.project.prefix;
				tmpData.children		= getDocStructLevel(arguments.dirQuery,arguments.project,"#arguments.path##arguments.delimiter##item#",arguments.page,arguments.delimiter);
				ArrayAppend(returnData,tmpData);
			</cfscript>			
		</cfloop>
		
		<cfloop query="qryElemenets">
			<cfscript>
				var tmpData 			= StructNew();
				tmpData.name			= ListFirst(qryElemenets.name,".");
				tmpData.type			= "page";
				tmpData.cls				= "#ListLast(qryElemenets.name,'.')#";
				tmpData.prefix			= arguments.project.prefix;
				tmpData.comp			= getDirPath(qryElemenets.directory & arguments.delimiter & qryElemenets.name,Replace(arguments.page.path,"{projectprefix}",arguments.project.prefix));
				ArrayAppend(returnData,tmpData);
			</cfscript>			
		</cfloop>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getDocNav" acc ess="public" returntype="string" output="false" hint="returns the navigation struct as html">
		<cfargument name="navStruct" required="true" type="struct" hint="nav struct">
		
		<cfset var returnData		= "">
		
		<cfsavecontent variable="returnData"><cfoutput>
			#getDocNavLevel(arguments.navStruct.children,arguments.navStruct.name)#
		</cfoutput></cfsavecontent>

		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getDocNavLevel" access="public" returntype="string" output="false" hint="returns the navigation children struct as html">
		<cfargument name="children" required="true" type="array" hint="nav struct">
		<cfargument name="page" required="true" type="string" hint="page">
		
		<cfset var index			= 0>
		<cfset var returnData		= "">
		
		<cfsavecontent variable="returnData"><cfoutput>
			<ul>
				<cfloop from="1" to="#ArrayLen(arguments.children)#" index="index">
					<li class="#arguments.children[index].type# #arguments.children[index].cls#">
						<cfif arguments.children[index].type eq "node">
							#arguments.children[index].name#
							#getDocNavLevel(arguments.children[index].children,arguments.page)#
						<cfelse>
							<a href="?project=#arguments.children[index].prefix#&amp;page=#arguments.page#&amp;view=#URLEncodedFormat(arguments.children[index].comp)#">#arguments.children[index].name#</a>	
						</cfif>
					</li>
				</cfloop>
				<cfif ArrayLen(arguments.children) eq 0>
					<li class="empty">No content</li>
				</cfif>
			</ul>
		</cfoutput></cfsavecontent>

		<cfreturn returnData />
	</cffunction>
	

</cfcomponent>