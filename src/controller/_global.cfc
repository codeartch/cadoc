<cfcomponent hint="global">


	<cffunction name="getCompName" access="public" returntype="string" output="false" hint="Returns the comp shorten name">
		<cfargument name="name" required="true" type="string" hint="Component name">
		
		<cfset var returnData = arguments.name>
		<cfset var returnData = ListRest(arguments.name,".")>
		<cfif ListFirst(returnData,".") eq "cacfcdoc">
			<cfset returnData = ListRest(returnData,".")>
		</cfif>
		
		<cfreturn returnData />
	</cffunction>	
	
	
	<cffunction name="getCompPath" access="public" returntype="string" output="false" hint="Returns the comp path of the given absolute path.">
		<cfargument name="directory" required="true" type="string" hint="Absolute Directory Path">
		<cfargument name="root" required="false" default="#request.project.path#" type="string" hint="Root Path">
		
		<cfset var returnData = arguments.directory>
		
		<cfset returnData = Replace(returnData,arguments.root,"","all")>
		<cfset returnData = Replace(returnData,"\","/","all")>
		<cfset returnData = Replace(returnData,"/",".","all")>
		<cfset returnData = Right(returnData,Len(returnData)-1)>
		<cfset returnData = ListDeleteAt(returnData,ListLen(returnData,"."),".")>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getDirPath" access="public" returntype="string" output="false" hint="Returns the dir path of the given absolute path.">
		<cfargument name="directory" required="true" type="string" hint="Absolute Directory Path">
		<cfargument name="root" required="false" default="#application.basePath#" type="string" hint="Root Path">
		
		<cfset var returnData = arguments.directory>
		
		<cfset returnData = Replace(returnData,arguments.root,"","all")>
		<cfset returnData = Replace(returnData,"\","/","all")>
		<cfset returnData = Right(returnData,Len(returnData)-1)>
		<cfset returnData = ListDeleteAt(returnData,ListLen(returnData,"."),".")>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="getPackagePath" access="public" returntype="string" output="false" hint="Returns the comp path of the given absolute path.">
		<cfargument name="comp" required="true" type="string" hint="Comp Path">

		<cfset returnData = Replace(arguments.comp,ListLast(arguments.comp,"."),"")>
		
		<cfif ListLen(arguments.comp) eq 1>
			<cfset returnData = "root">
		</cfif>
		
		<cfreturn returnData />
	</cffunction>
	
	
	<cffunction name="ArrayOfStructSort" returntype="array" access="public">
		<cfargument name="base" type="array" required="yes" />
		<cfargument name="sortType" type="string" required="no" default="text" />
		<cfargument name="sortOrder" type="string" required="no" default="ASC" />
		<cfargument name="pathToSubElement" type="string" required="no" default="" />
		
		<cfset var tmpStruct = StructNew()>
		<cfset var returnVal = ArrayNew(1)>
		<cfset var i = 0>
		<cfset var keys = "">
		
		<cfloop from="1" to="#ArrayLen(base)#" index="i">
			<cfset tmpStruct[i] = base[i]>
		</cfloop>
		
		<cfset keys = StructSort(tmpStruct, sortType, sortOrder, pathToSubElement)>
		
		<cfloop from="1" to="#ArrayLen(keys)#" index="i">
			<cfset returnVal[i] = tmpStruct[keys[i]]>
		</cfloop>
		
		<cfreturn returnVal>
	</cffunction>

	

</cfcomponent>