<cfif not IsDefined("url.ajaxrequest")>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en" />
		
		<meta name="Keywords" content="codeart,docs,cfc,object,documentation,help" />
		<meta name="Description" content="Codeart Doc is a documentation tool for Projects with Adobe Coldfusion" />
		<title><cfoutput>#application.config.pageTitel#</cfoutput></title>
		
		<!-- JQuery -->
		<script type="text/javascript" src="src/media/lib/jquery_183/jquery.js"></script>
		
		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="src/media/lib/bootstrap_221/css/bootstrap.min.css">
		<script type="text/javascript" src="src/media/lib/bootstrap_221/js/bootstrap.min.js"></script>
		
		<!-- Prettify -->
		<link type="text/css" rel="stylesheet" href="src/media/lib/prettify_1/prettify.css">
		<script type="text/javascript" src="src/media/lib/prettify_1/prettify.js"></script>
		
		<!-- Custom -->
		<link type="image/x-icon" rel="shortcut icon" href="src/media/img/favicon.ico" />
		<link type="text/css" rel="stylesheet" href="src/media/css/layout.css">
		<script type="text/javascript" src="src/media/js/main.js"></script>
		<script type="text/javascript" src="src/media/js/expand.js"></script>
		
	</head>
	<body>	
		<div class="navbar navbar-inverse navbar-fixed-top top-wrap">
			<div class="navbar-inner">
				<div class="container-fluid">
					
					<cfoutput>
						<a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<a class="brand" href="index.cfm" target="_blank">Documentation</a>
						<div class="nav-collapse">
							#request.projectNav#
						</div>
						
						<ul class="nav pull-right">
							<li class="navbar-text">Version #application.version# (#application.versionName#)</li>
							<cfif application.config.lastUpdated eq "init">
								<li class="divider-vertical"></li>
								<li class="navbar-text">Last update: #LSDateFormat(application.created)#-#LSTimeFormat(application.created)#</li>
							<cfelseif application.config.lastUpdated neq "off">
								<li class="divider-vertical"></li>
								<li class="navbar-text">Last update: #application.config.lastUpdated#</li>
							</cfif>
						</ul>
					</cfoutput>
					
				</div>
			</div>
		</div>
		
		<div class="container-fluid main-wrap">
			<div class="row-fluid">
				<cfoutput>
					<div class="spancustom1 sidebar">
						#request.sidebar#&nbsp
					</div>
			
					<div class="spancustom2 content #request.url.page#" id="content">
						#request.content#
					</div>
				</cfoutput>
			</div>
		</div>
	</body>
	</html>
<cfelse>
	<cfoutput>
		#request.content#
	</cfoutput>	
</cfif>



