<cfcomponent displayname="Application" output="false">
	<cfsetting
        requesttimeout="180"
        showdebugoutput="false"
        enablecfoutputonly="false"
    />

	<!--- application setting --->
	<cfset this.name 					= "cacfcdocs8">
	<cfset this.sessionmanagement		= false>
	<cfset this.clientmanagement		= false>
	<cfset this.applicationtimeout 		= CreateTimeSpan(1,0,0,0)>

	<!--- application mapping --->
	<cfset this.mappings["/webroot"] 	= ExpandPath("/")>
	<cfset this.mappings["/approot"] 	= ExpandPath("./")>


    <cffunction name="onApplicationStart" returntype="void" output="no" hint="This fires when the application first runs. It is a single-threaded method call">
		
		<cfset var item 				= StructNew()>
		<cfset var item_m 				= StructNew()>
		<cfset var item_p 				= StructNew()>
		
		<cfset application.created		= Now()>
		<cfset application.listStore 	= StructNew()>
		<cfset application.fileStore 	= StructNew()>
		<cfset application.basePath 	= ExpandPath("/approot")>
		<cfif FindNoCase("localhost", CGI.SERVER_NAME)>
			<cfset application.configXml	= XmlParse("#application.basePath#/user_data/config/config_development.xml.cfm")>
		<cfelse>
			<cfset application.configXml	= XmlParse("#application.basePath#/user_data/config/config_production.xml.cfm")>
		</cfif>
		<cfset application.projectCfg	= XmlSearch(application.configXml,"/config/project/item")>
		<cfset application.version		= Trim(GetProfileString("#GetDirectoryFromPath(GetCurrentTemplatePath())#/version.ini.cfm", "Basic", "version"))>
		<cfset application.versionName	= Trim(GetProfileString("#GetDirectoryFromPath(GetCurrentTemplatePath())#/version.ini.cfm", "Basic", "versionname"))>
		
		<!--- set components --->
		<cfset application.controller				= StructNew()>
		<cfset application.controller.File			= CreateObject('component','src.controller.File')>
		<cfset application.controller.Util			= CreateObject('component','src.controller.Util')>
		<cfset application.controller.Comp			= CreateObject('component','src.controller.Comp')>
		<cfset application.controller.Content		= CreateObject('component','src.controller.Content').init("#application.basePath#/user_data/content/data.json")>
		
		<!--- set config --->
		<cfset application.config 					= StructNew()>
		<cfscript>
			application.config.pageTitel			= Trim(application.configXml.config.pageTitle.xmlText);
			application.config.lastUpdated			= Trim(application.configXml.config.lastUpdated.xmlText);
			application.config.editMode				= Trim(application.configXml.config.editMode.xmlText);
			application.config.stickOnTop			= Trim(application.configXml.config.stickOnTop.xmlText);
			application.config.delimiter			= Trim(application.configXml.config.delimiter.xmlText);
			application.config.project				= ArrayNew(1);
			application.config.projectPrefix		= StructNew();
		</cfscript>
		
		<cfloop array="#application.projectCfg#" index="item">
			<cfif Trim(item.active.xmlText)>
				<cfset var package					= StructNew()>
				<cfset var path						= Trim(item.path.xmlText)>
				<cfset var mapping					= StructNew()>
				<cfset var mappingCfg				= XmlSearch(item,"mapping/item")>
				<cfset package.mapping				= ArrayNew(1)>
				
				<cfset package.name 				= Trim(item.name.xmlText)>
				<cfif Left(path,1) eq ".">
					<cfset package.path 			= ExpandPath(path)>
				<cfelse>
					<cfset package.path 			= path>
				</cfif>
				<cfset package.prefix 				= Trim(item.prefix.xmlText)>
				
				<cfset package.module.doc 			= Trim(item.module.doc.xmlText)>
				<cfset package.module.altdoc 		= Trim(item.module.altdoc.xmlText)>
				<cfset package.module.guide 		= Trim(item.module.guide.xmlText)>
				<cfset package.module.altguide 		= Trim(item.module.altguide.xmlText)>
				<cfset package.module.manual 		= Trim(item.module.manual.xmlText)>
				<cfset package.module.altmanual 	= Trim(item.module.altmanual.xmlText)>
				<cfset package.module.download 		= Trim(item.module.download.xmlText)>
				<cfset package.module.altdownload	= Trim(item.module.altdownload.xmlText)>
				<cfset package.module.demo 			= Trim(item.module.demo.xmlText)>
				<cfset package.module.altdemo		= Trim(item.module.altdemo.xmlText)>
				
				<cfset package.filter 				= Trim(item.filter.xmlText)>
				<cfset package.recurse 				= Trim(item.recurse.xmlText)>
				<cfset package.globalfile 			= Trim(item.globalfile.xmlText)>
				<cfset package.constructor	 		= Trim(item.constructor.xmlText)>
				<cfset package.onready		 		= Trim(item.onready.xmlText)>
				<cfset package.hidePrivateMethods 	= Trim(item.hidePrivateMethods.xmlText)>
				
				<!--- set docMapping --->
				<cfloop array="#mappingCfg#" index="item_m">
					<cfset mapping 		= StructNew()>
					<cfset mapping.name = item_m.name.xmlText>
					<cfset mapping.path = item_m.path.xmlText>
					
					<cfset ArrayAppend(package.mapping,mapping)>
				</cfloop>
				
				<cfset ArrayAppend(application.config.project,package)>
				<cfset application.config.projectPrefix[package.prefix] = package>
			</cfif>
		</cfloop>
		
		<!--- set page--->
		<cfset application.page = StructNew()>
		<cfloop list="doc,guide,manual,download,demo" index="item_p">
			<cfset application.page[item_p]["key"] 	= item_p>
			<cfset application.page[item_p]["path"]	= "#application.basePath##application.config.delimiter#user_data#application.config.delimiter#content#application.config.delimiter#{projectprefix}#application.config.delimiter##item_p#">
		</cfloop>
		
		<!--- set listStore cfcdoc--->
		<cfloop array="#application.config.project#" index="item">
			<cfset application.listStorePackage[item.prefix]	= application.controller.file.listByProject(item)>
		</cfloop>
		
		<!--- set listStore doc --->
		<cfloop array="#application.config.project#" index="item">
			<cfloop collection="#application.page#" item="item_p">
				<cfset application.listStorePage[item.prefix][item_p] = application.controller.file.listByPath(Replace(application.page[item_p].path,"{projectprefix}",item.prefix))>
			</cfloop>
		</cfloop>
		
		<cfset application.controller.Util.init(application.listStorePackage,application.listStorePage)>
		<cfset application.controller.Comp.init(application.listStorePackage)>
		
    	<cfreturn />
    </cffunction>
	
	
 	<cffunction name="onRequestStart" returntype="boolean" output="true" hint="This fires when a page request first runs. It is a single-threaded method call">
		<cfargument name="TargetPage" type="string" required="yes">
		
		<cfset var item 	= StructNew()>
		<cfset var item_m	= StructNew()>
		
		<!--- reload application --->
		<cfif IsDefined("url.init")>
			<cfset onApplicationStart()>
			<!--- <cflocation url="index.cfm" addtoken="no"> --->
		</cfif>
		
		<!--- error --->
		<cfif StructIsEmpty(application.config.projectPrefix)>
			<p>Error: No project found.</p><cfabort>
		</cfif>
		
		<!--- mapping --->
		<cfset this.mappings				= StructNew()>
		<cfset this.mappings["/webroot"] 	= ExpandPath("/")>
		<cfset this.mappings["/approot"] 	= ExpandPath("./")>
		
		<cfloop array="#application.config.project#" index="item">
			<cfset this.mappings["/doc_#item.prefix#"]		= item.path>

			<cfloop array="#item.mapping#" index="item_m">
				<cfset this.mappings["/#item_m.name#"]		= item_m.path>
			</cfloop>
		</cfloop>
		
		<!--- URL vars --->
		<cfset request.url = StructNew()>
		
		<cfset request.url.project = application.config.project[1].prefix>
		<cfif IsDefined("url.project")>
			<cfif StructKeyExists(application.listStorePackage,url.project)>
				<cfset request.url.project = Trim(url.project)>
			<cfelse>
				<cflocation url="?view=notfound" addToken="no">
			</cfif>
		</cfif>
		
		<cfset request.url.page = "">
		<cfif IsDefined("url.page")>
			<cfif StructKeyExists(application.page,url.page)>
				<cfset request.url.page = Trim(url.page)>
			<cfelse>
				<cfset request.url.page = "">
			</cfif>
		</cfif>
		
		<cfset request.url.view = "">
		<cfif IsDefined("url.view")>
			<cfset request.url.view = Replace(Replace(Trim(url.view),"../","","all"),"./","","all")>
		</cfif>
		
		<cfset request.url.comp = "">
		<cfif IsDefined("url.comp")>
			<cfset request.url.comp = Trim(url.comp)>
		</cfif>
		
		<cfset request.url.tab = "">
		<cfif IsDefined("url.tab")>
			<cfset request.url.tab = Trim(url.tab)>
		</cfif>
		
		<!--- <cfset request.url.call = "page=#request.url.page#&amp;view=#request.url.view#&amp;comp=#request.url.comp#&amp;tab=#request.url.tab#"> --->
		<cfset request.url.call = CGI.QUERY_STRING>
		
		<!--- vars project --->
		<cfset request.project			= application.config.projectPrefix[request.url.project]>
		<cfset request.projectArray		= application.config.project>
		<cfset request.projectNav		= application.controller.Util.getProjectNav(request.projectArray,request.project.prefix)>
		
		<cfif Len(request.url.page) gte 1>
			
			<!--- vars page --->
			<cfset request.page				= application.page[request.url.page]>
			
			<cfset request.listStorePackage	= application.listStorePackage[request.url.project]>
			<cfif StructKeyExists(application.listStorePage[request.url.project],request.url.page)>
				<cfset request.listStorePage		= application.listStorePage[request.url.project][request.url.page]>
			<cfelse>
				<cfset request.listStorePage		= QueryNew("")>
			</cfif>
			
			<cfset request.format			= application.controller.Util.formatCode>
			<cfset request.packageSruct		= application.controller.Util.getPackageStruct(request.project)>
			<cfset request.packageNav		= application.controller.Util.getPackageNav(request.packageSruct)>
			<cfset request.documentSruct	= application.controller.Util.getDocStruct(request.project,request.page)>
			<cfset request.documentNav		= application.controller.Util.getDocNav(request.documentSruct)>
			
			<!--- content --->
			<cfif application.config.editMode>
				<cfset application.controller.Content.reloadJsonData()>
			</cfif>
			
			<cftry>
				<cfinclude template="src/view/page/#request.url.page#.cfm">		
				<cfcatch type="missingInclude">
					<cfinclude template="src/view/page/notfound.cfm">
				</cfcatch>
			</cftry>
		<cfelse>
			<cfinclude template="src/view/page/startpage.cfm">
		</cfif>
		
    	<cfreturn true />
    </cffunction>

	
	<cffunction name="onRequest" returntype="void"  output="true" hint="This fires when the requested template needs to get processed">
		<cfargument name="TargetPage" type="string" required="yes" />
			
			<cfinclude template="#arguments.targetpage#" />
		<cfreturn />
	</cffunction>
	
		
	<cffunction name="onError" returntype="void" hint="This fires if an exception gets thrown and is not caught by the controlling code" output="yes">
		<cfargument name="exception" type="any" required="yes">
		<cfargument name="eventName" type="string" required="no" default="">

		<!--- <cfdump var="#arguments.exception#"><cfabort> --->
		<h1>Error</h1><cfabort>
		
		<cfreturn />
    </cffunction>
	
</cfcomponent>