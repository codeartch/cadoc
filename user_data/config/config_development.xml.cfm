<config>
	<pageTitle>Codeart Documentation</pageTitle>
	<lastUpdated>off</lastUpdated><!-- off/init/28.12.2020 -->
	<editMode>false</editMode>
	<stickOnTop>true</stickOnTop>
	<delimiter>/</delimiter>

	<project>
		<item>
			<name>cadmin</name>
			<path>/Volumes/Web/wwwroot/cadmin</path>
			<prefix>cadmin</prefix>
			<active>true</active>
			
			<module>
				<doc>true</doc>
				<altdoc></altdoc>
				<guide>true</guide>
				<altguide></altguide>
				<manual>true</manual>
				<altmanual></altmanual>
				<download>false</download>
				<altdownload></altdownload>
				<demo>false</demo>
				<altdemo></altdemo>
			</module>
			
			<mapping>
				<item>
					<name>cadminmaster</name>
					<path>/Volumes/Web/wwwroot/cadmin/master/camaster</path>
				</item>
				<item>
					<name>cadminmasterroot</name>
					<path>/Volumes/Web/wwwroot/cadmin/master</path>
				</item>
				<item>
					<name>cadminclient</name>
					<path>/Volumes/Web/wwwroot/cadmin/client01/caclient01</path>
				</item>
				<item>
					<name>cadminclientroot</name>
					<path>/Volumes/Web/wwwroot/cadmin/client01</path>
				</item>
			</mapping>
			
			<filter>*.cfc</filter>
			<recurse>true</recurse>
			<globalfile>global.cfc</globalfile>
			<constructor>init</constructor>
			<onready>default</onready>
			<hidePrivateMethods>false</hidePrivateMethods>
		</item>
		<item>
			<name>cadmin 2</name>
			<path>/Volumes/Web/wwwroot/cadmin2/master</path>
			<prefix>cadmin2</prefix>
			<active>false</active>
			
			<module>
				<doc>true</doc>
				<altdoc></altdoc>
				<guide>true</guide>
				<altguide></altguide>
				<manual>true</manual>
				<altmanual></altmanual>
				<download>true</download>
				<altdownload></altdownload>
				<demo>true</demo>
				<altdemo></altdemo>
			</module>
			
			<filter>*.cfc</filter>
			<recurse>true</recurse>
			<globalfile>_global.cfc</globalfile>
			<constructor>init</constructor>
			<onready>default</onready>
			<hidePrivateMethods>false</hidePrivateMethods>
		</item>
		<item>
			<name>caevent</name>
			<path>/Volumes/Web/wwwroot/caevent/cfc</path>
			<prefix>caevent</prefix>
			<active>true</active>
			
			<module>
				<doc>true</doc>
				<altdoc></altdoc>
				<guide>true</guide>
				<altguide></altguide>
				<manual>false</manual>
				<altmanual></altmanual>
				<download>true</download>
				<altdownload>http://caevent.riaforge.org/index.cfm?event=action.download</altdownload>
				<demo>true</demo>
				<altdemo></altdemo>
			</module>
			
			<filter>*.cfc</filter>
			<recurse>true</recurse>
			<globalfile>_global.cfc</globalfile>
			<constructor>init</constructor>
			<onready>default</onready>
			<hidePrivateMethods>false</hidePrivateMethods>
		</item>
	</project>
</config>
