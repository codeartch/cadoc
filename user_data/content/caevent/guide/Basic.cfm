<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-warning">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Requirements</h4>
	OS: Linux / OSX / Windows<br />
	Coldfusion 8+, Railo 3.1+<br />
</div>


<cfoutput>
<p>CAEvent provides a simple event driven framework to allow a more event-based, asynchronous approach to developing ColdFusion applications.<br />
The structure of the framework is based on AS3 and Javascript event handling.</p>

<h2>Installation</h2>
<pre>
#request.format('
<cfset application.caevent = StructNew()>
<cfset application.caevent.eventManager	= CreateObject("component","cfc.caevent.EventManager").init()>
')#
</pre>


<h2>Creating a Event</h2>
<pre>
#request.format('
<cfset e = application.caevent.EventManager.new("example2_event")>
')#
</pre>


<h2>Add listeners</h2>
<pre>
#request.format('
<cfset e.addListener(application.exampleObj,"run1")>
')#
</pre>

<h2>Dispatch event</h2>
<pre>
#request.format('
<cfset returndata = e.dispatch({name="edie",lastname="britt"})>
')#
</pre>

<p>Dispatch the event with two parameters (name="edie",lastname="britt") <br /> The parameters are passed to the listeners.</p>
</cfoutput>