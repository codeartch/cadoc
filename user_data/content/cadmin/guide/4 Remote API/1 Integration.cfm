<cfoutput>
<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-info">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Remember</h4>
	<ul>
		<li>The API functions are only implemented in CAdmin version 1.910 and higher.</li>
	</ul>
</div>


<h3>Placeholder</h3>
<p>Change the {APIURL} Placeholder to your CAdmin API URL (e.g. "http://demo.codeart.ch/cadmin/client01/caclient01/cfc/api") </p>
<p>Change the {TOKEN} Placeholder to your CAdmin user token (e.g. "DC9E0C99-0619-50F3-277CC3233A55BBE1"):</p>


<h3>Coldfusion WSDL</h3>

<p>This Example shows how to integrate the CAdmin remote API as a webservice (WSDL):</p>

<pre class="nospace prettyprint">
	#request.format('<cfset rndNumber = CreateObject("webservice", "{APIURL}/remoteService.cfc?wsdl").GetRandomNumber({TOKEN})>')#
</pre>

<pre class="prettyprint">
#request.format('
<cfinvoke webservice="{APIURL}/remoteService.cfc?wsdl" refreshWSDL="yes" method="GetRandomNumber" returnvariable="rndNumber">
	<cfinvokeargument name="accessToken" value="{TOKEN}">
</cfinvoke>
')#
</pre>
 	

<h3>Javascript (Ajax)</h3>

<p>This Example shows how to call the CAdmin remote API with Ajax (Json):</p>

<pre class="prettyprint">
#request.format('	
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">		
	// Ajax Call auf Remote CFC
	function GetRandomNumber() {
		$.ajax(
			{
				method: "get",
				url: "{APIURL}/remoteService.cfc",
				data: {method: "GetRandomNumber", accessToken: "{TOKEN}"},
				dataType: "json",
				success:function(objResponse){
					console.log(objResponse);
				}
			}
		);
	}
</script>
')#
</pre>

</cfoutput>