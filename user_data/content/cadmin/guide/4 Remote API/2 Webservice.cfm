<cfoutput>
<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-info">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Remember</h4>
	<ul>
		<li>The API functions are only implemented in CAdmin version 1.910 and higher.</li>
	</ul>
</div>

<h3>Example API Method</h3>

<p>This is the example API method of the CAdmin master instance:</p>

<pre class="prettyprint">
#request.format('
<cffunction name="GetRandomNumber" access="remote" returntype="struct" returnformat="json" output="false" hint="I return a random value.">
	<cfargument name="accessToken" required="yes" type="string" hint="Access Token">
	 
	<cfset init()>
	<cfset local.returnData 	= StructNew()>
		
	<cfif hasAccess(arguments.accessToken)>
		<cfset local.returnData["success"]	= true>
		<cfset local.returnData["data"]		= RandRange(1,100)>
		<cfset local.returnData["message"]	= "">
	<cfelse>
		<cfset local.returnData["success"]	= false>
		<cfset local.returnData["data"]		= "">
		<cfset local.returnData["message"]	= "Invalid Token">
	</cfif>

	<cfreturn local.returnData />
</cffunction>
')#
</pre>


<h3>Customization</h3>
<p>You can create your own API service by creating a new method in the remoteService.cfc component of your client instance or module.</p>
<p>This method has access to all system informations (e.g. user,group,lang,module…). Be sure to set access=remote so its accessible as a webservice.</p>


</cfoutput>