<cfoutput>
<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-info">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Remember</h4>
	<ul>
		<li>If you make changes in the master instance, all client instances will also be affected.</li>
		<li>By default all client components extends the master ones.</li>
	</ul>
</div>

<h3>Master</h3>

<p>This structure shows the basic information about the most parts of the system:</p>

<ul>
	<li><span class="label">camaster</span> Secret directory which contains the main source
		<ul>
			<li><span class="label label-info">cfc</span>
				<ul>
					<li><span class="label">api</span> Contains components which are remotely accessible</li>
					<li><span class="label">core</span> System core components</li>
					<ul>
						<li><span class="label">controller</span> Contains components which task is to do some logic stuff with no user output</li>
						<li><span class="label">renderer</span> Contains components which task is to output HTML/JS... content</li>
					</ul>
					<li><span class="label">gateway</span> Contains components which task ist to create a facade for session/orm... objects</li>
					<li><span class="label">general</span> Contains some general system components</li>
					<li><span class="label">io</span> Contains components which task is to create a input/output stream between the storage and the interface. Returns mostly JSON content</li>
					<li><span class="label">lib</span> Contains 3th part components</li>
					<li><span class="label">factories</span> Components which task is to assign and inject all system components</li>
				</ul>
			</li>
			<li><span class="label">media</span>
				<ul>
					<li><span class="label">css</span> Contains CSS files</li>
					<li><span class="label">img</span> Contains image files</li>
					<li><span class="label">js</span> Contains JS files</li>
					<li><span class="label">lib</span> Contains 3th part files e.g. JQuery,ExtJS..</li>
					<li><span class="label">sound</span> Contains sound files</li>
				</ul>
			</li>
			<li><span class="label label-info">view</span> System views are located in this directory</li>
		</ul>
	</li>
	<li><span class="label">login</span> Unprotected directory for unauthorized users</li>
	<li><span class="label label-info">model</span> In this directory are all system model files. Hibernate creates automatically database tables from this models</li>
</ul>

<h3>MVC</h3>
<p>The directories marked as <span class="label label-info">blue</span> declaring the different parts of the MVC pattern.</p>
<ul>
	<li>model = model</li>
	<li>view = view</li>
	<li>controller = cfc</li>
</ul>

</cfoutput>