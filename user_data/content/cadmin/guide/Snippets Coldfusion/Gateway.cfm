<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<cfoutput>
<h3>ORM Gateway</h3>
<div id="ormPart">
	<p>Create a new object (like EntityNew, you can pass in a struct with keys and values pairs):</p>
	<pre class="nospace">
		#request.format("<cfset local.createBean = variables.gateway.orm.systemLogdata.new(valuestruct)>")#
	</pre>
	<p>Save a new orm object (like EntitySave, only needed if the object not already exists in the database):</p>
	<pre class="nospace">
		#request.format("<cfset variables.gateway.orm.systemLogdata.save(local.createBean)>")#
	</pre>
	<p>Reload a new orm object (like EntityReload):</p>
	<pre class="nospace">
		#request.format("<cfset variables.gateway.orm.systemLogdata.reload(local.createBean)>")#
	</pre>
	<p>Remove a orm object (like EntityDelete):</p>
	<pre class="nospace">
		#request.format("<cfset variables.gateway.orm.systemLogdata.delete(local.createBean)>")#
	</pre>
	<p>Create a new entity, saves it and returns the object (you can pass in a struct with keys and values pairs):</p>
	<pre class="nospace">
		#request.format("<cfset local.newSavedBean = variables.gateway.orm.systemLogdata.saveNew(valuestruct)>")#
	</pre>
	<h4>ORM Gateway Getters</h4>
	<p>There are custom getters aswell (e.g. getById, getAll, getAllActive…). Please use the CFC documentation for more information about these.</p>
</div>

</cfoutput>