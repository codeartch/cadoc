<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">
<cfinclude template="/approot/src/view/object/alert/usecfcdoc.cfm">

<cfoutput>
<h3>System</h3>
<h4>Language</h4>
<div id="langPart">
	<p>Returns the requested langkey in the default system language:</p>
	<pre class="nospace">
		#request.format("<cfset local.key = variables.core.controller.systemLang.getDefaultContentKey('login_resetpw_title')>")#
	</pre>
	<p>Returns the requested langkey in the current user language:</p>
	<pre class="nospace">
		#request.format("<cfset local.key = variables.core.controller.systemLang.getCurrentContentKey('login_resetpw_title')>")#
	</pre>
	
	<p>Returns the lang struct in the default system language:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getDefaultContentStruct()>")#
	</pre>
	<p>Returns the lang struct in the current user language:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getCurrentContentStruct()>")#
	</pre>
</div>

<h4>Log</h4>
<div id="logPart">
	<p>Returns the requested logkey:</p>
	<pre class="nospace">
		#request.format("<cfset local.key = variables.core.controller.systemLang.getLogdataContentKey('entity_move')>")#
	</pre>
	
	<p>Returns the log struct:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getLogdataContentStruct()>")#
	</pre>
</div>

<h4>Error</h4>
<div id="errorPart">
	<p>Returns the requested errorkey:</p>
	<pre class="nospace">
		#request.format("<cfset local.key = variables.core.controller.systemLang.getErrorContentKey('nolang')>")#
	</pre>
	
	<p>Returns the log struct:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getErrorContentStruct()>")#
	</pre>
</div>

<hr />
<h3>Module</h3>
<div id="modulePart">
	<p>Returns the requested admin langkey in the default system language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.key = variables.core.controller.systemLang.getDefaultModuleAdminKey(systemModule, key)>")#
	</pre>
	
	<p>Returns the requested admin langkey in the current user language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.key = variables.core.controller.systemLang.getCurrentModuleAdminKey(systemModule, key)>")#
	</pre>
	
	<p>Returns the requested public langkey in the given language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.key = variables.core.controller.systemLang.getModulePublicKey(systemModule,systemLang, key)>")#
	</pre>
	
	
	<p>Returns a array which contains all admin language keys in the default system language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getDefaultModuleAdminContent(systemModule)>")#
	</pre>
	
	<p>Returns a array which contains all admin language keys in the current user language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getCurrentModuleAdminContent(systemModule)>")#
	</pre>
	
	<p>Returns a array which contains all public language keys in the given language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getCurrentModuleAdminContent(systemModule,systemLang)>")#
	</pre>
	
	<p>Returns a struct which contains all admin language keys in the default system language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getDefaultModuleAdminContentStruct(systemModule)>")#
	</pre>
	
	<p>Returns a struct which contains all admin language keys in the current user language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getCurrentModuleAdminContentStruct(systemModule)>")#
	</pre>
	
	<p>Returns a struct which contains all public language keys in the given language for the given module:</p>
	<pre class="nospace">
		#request.format("<cfset local.struct = variables.core.controller.systemLang.getCurrentModuleAdminContentStruct(systemModule,systemLang)>")#
	</pre>
</div>


</cfoutput>