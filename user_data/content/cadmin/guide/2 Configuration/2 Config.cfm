<cfoutput>
<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-info">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Remember</h4>
	<ul>
		<li>Initialize the application to see your changes take effect.</li>
		<li>If you make changes in the master instance, all client instances will also be affected.</li>
	</ul>
</div>

<p>The config.ini file is located in the root of each cadmin instance (master/client).</p>

<h3>Status</h3>
<div id="statusPart">
	<p>Is cadmin already installed. If "false" the installation starts with the next request (Attention: the database gets truncated)</p>
	<pre class="nospace">
		#request.format("installed=false")#
	</pre>
</div>

<h3>Basic</h3>
<div id="basicPart">
	<p>API Key, used for the remote connection to get system configuration data</p>
	<pre class="nospace">
		#request.format("api_key=03E05AF1-FF27-C852-53B1B8B91XXXXXXX")#
	</pre>
	<p>API Url, location to the webservice</p>
	<pre class="nospace">
		#request.format("api_url=http://www.codeart.ch")#
	</pre>
	<p>Titel of the client application. Appears on the browser title,login screen admin interface</p>
	<pre class="nospace">
		#request.format("title=Codeart CLIENT Admin")#
	</pre>
	<p>If the indicator is found in CGI.SERVER_NAME the local settings are load, otherwise the remote settings are load</p>
	<pre class="nospace">
		#request.format("indicator=localhost")#
	</pre>
	<p>Name of the cadmin root directory (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("root=cadmin")#
	</pre>
	<p>Name of the client directory (In most cases this is the project name e.g. "nike")</p>
	<pre class="nospace">
		#request.format("dir=client01")#
	</pre>
	<p>Name of the secret directory (e.g. "n1ke2012")</p>
	<pre class="nospace">
		#request.format("dir_secret=ca1")#
	</pre>
	<p>Name of the login directory (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("dir_login=login")#
	</pre>
	<p>Name of the secret directory (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("dir_module=module")#
	</pre>
	<p>Name of the error directory (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("dir_error=error")#
	</pre>
	<p>List which contains restricted directories (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("dir_protected=caclient01,module,error")#
	</pre>
	<p>Coldfusion error template</p>
	<pre class="nospace">
		#request.format("template_cferror=view/cfError.cfm")#
	</pre>
	<p>Cadmin client error template</p>
	<pre class="nospace">
		#request.format("template_clienterror=view/caClientError.cfm")#
	</pre>
	<p>Cadmin system error template</p>
	<pre class="nospace">
		#request.format("template_systemerror=view/caSystemError.cfm")#
	</pre>
	<p>Error index file</p>
	<pre class="nospace">
		#request.format("template_errorindex=index.cfm")#
	</pre>	
</div>

<h3>Local / Remote</h3>
<div id="remotePart">
	<p>License key (Could be optional, depends on the webservice your using)</p>
	<pre class="nospace">
		#request.format("license=XXXXXXXX-XXXX-XXXX-XXXXXXXXXXXXXXXX")#
	</pre>
	<p>Path to the frontend. Used for preview links</p>
	<pre class="nospace">
		#request.format("frontend=http://ca/cadmin_frontend")#
	</pre>
	<p>Datasource name</p>
	<pre class="nospace">
		#request.format("ds_name=cadmin_client01")#
	</pre>
	<p>Datasource dialect (For more details look at the cf9/hibernate manual)</p>
	<pre class="nospace">
		#request.format("ds_dialect=MySQLwithInnoDB")#
	</pre>
	<p>Datasource mode (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("ds_mode=update")#
	</pre>
	<p>Ip-Address or domain of the smtp server</p>
	<pre class="nospace">
		#request.format("mail_smtp_server=127.0.0.1")#
	</pre>
	<p>Smtp-Server username (Can be empty if no username is required)</p>
	<pre class="nospace">
		#request.format("mail_smtp_username=username")#
	</pre>
	<p>Smtp-Server password (Can be empty if no password is required)</p>
	<pre class="nospace">
		#request.format("mail_smtp_password=secret123")#
	</pre>
	<p>Is the developer mode active. Developer mode means: No error mail, init functions improved for development, uncompressed media files</p>
	<pre class="nospace">
		#request.format("mode_dev=true")#
	</pre>
	<p>Is the inactive mode active. Inactive mode means: Login gets disabled an online user get kicked</p>
	<pre class="nospace">
		#request.format("mode_inactive=false")#
	</pre>
</div>


<h3>Setting</h3>
<div id="settingPart">
	<h4>General</h4>
	<p>This key is needed to initialize the application in remote mode</p>
	<pre class="nospace">
		#request.format("general_initkey=casecureinit")#
	</pre>
	<p>Sets how many informations are get logged if an error occurs</p>
	<ul>
		<li>0=Exception</li>
		<li>1=Exception,Cgi,Form,Url</li>
		<li>2=Exception,Cgi,Form,Url,Request,Session,Application</li>
	</ul>
	<pre class="nospace">
		#request.format("general_errordetail=1")#
	</pre>
	<p>After how many unsuccessful tries the ip lock takes action</p>
	<pre class="nospace">
		#request.format("general_ip_locklimit=5")#
	</pre>
	<p>After how many minutes unsuccessful attempts gets cleared</p>
	<pre class="nospace">
		#request.format("general_ip_releasetime=10")#
	</pre>
	<p>How many minutes does the ip-lock last.</p>
	<pre class="nospace">
		#request.format("general_ip_locktime=120")#
	</pre>
	<p>Should modules automatically get reloaded (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("general_reload_module=false")#
	</pre>
	<p>Timeout after modules are reloaded. Increases the system performance (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("general_reload_module_timeout=300")#
	</pre>
	<p>Should the admin navigation reload after every request (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("general_reload_nav=false")#
	</pre>
	<p>Should the session of online users be validated</p>
	<pre class="nospace">
		#request.format("general_check_session=true")#
	</pre>
	<p>Timeout after session validation. Increases the system performance</p>
	<pre class="nospace">
		#request.format("general_check_session_timeout=300")#
	</pre>
	<p>Available system languages (Depends on the webservice you are using)</p>
	<pre class="nospace">
		#request.format("general_applang=keys,de")#
	</pre>
	<p>Module which gets initialized on application start</p>
	<pre class="nospace">
		#request.format("general_moduleinit=sitemanager,smssuite")#
	</pre>
	<p>Should the io content get returned in the json mine type format</p>
	<pre class="nospace">
		#request.format("general_contentjsonformat=false")#
	</pre>
	<p>User can be logged in from different ip-addresses at the same time</p>
	<pre class="nospace">
		#request.format("general_allowmultiuserlogins=true")#
	</pre>
	<p>Time in seconds to enable the admin action bar after a request is completed (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("general_linkenabletimeout=100")#
	</pre>
	<p>Page encoding format (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("general_codierung=UTF-8")#
	</pre>
	<p>Startpage template (located in the view folder)</p>
	<pre class="nospace">
		#request.format("general_template_startpage=startpage.cfm")#
	</pre>
	<p>Should edit tabs get closed on save</p>
	<pre class="nospace">
		#request.format("general_closetab_onsave=true")#
	</pre>
	<p>Should add tabs get opened as edit after save</p>
	<pre class="nospace">
		#request.format("general_openedittab_onadd=false")#
	</pre>
	<h4>General Log</h4>
	<p>Create a log entry for load action (Should only be changed by experienced developers)</p>
	<pre class="nospace">
		#request.format("general_log_load=false")#
	</pre>
	<p>Create a log entry for create action</p>
	<pre class="nospace">
		#request.format("general_log_create=true")#
	</pre>
	<p>Create a log entry for update action</p>
	<pre class="nospace">
		#request.format("general_log_update=true")#
	</pre>
	<p>Create a log entry for move action</p>
	<pre class="nospace">
		#request.format("general_log_move=true")#
	</pre>
	<p>Create a log entry for sort action</p>
	<pre class="nospace">
		#request.format("general_log_sort=true")#
	</pre>
	<p>Create a log entry for delete action</p>
	<pre class="nospace">
		#request.format("general_log_delete=true")#
	</pre>
	<p>Create a log entry for uplaod action</p>
	<pre class="nospace">
		#request.format("general_log_upload=true")#
	</pre>
	<h4>General Upload</h4>
	<p>List of runtimes used by the multiupload</p>
	<pre class="nospace">
		#request.format("general_upload_runtime=gears,silverlight,html5,html4")#
	</pre>
	<p>Maximum file size for the multiupload</p>
	<pre class="nospace">
		#request.format("general_upload_maxfilesize=30mb")#
	</pre>
	<h4>General Date/Time</h4>
	<p>System dateformat</p>
	<pre class="nospace">
		#request.format("general_dateformat=dd.mm.yyyy")#
	</pre>
	<p>System timeformat</p>
	<pre class="nospace">
		#request.format("general_timeformat=HH:mm:ss")#
	</pre>
	<p>Simple system dateformat</p>
	<pre class="nospace">
		#request.format("general_dateformat_simple=ddmmyyyy")#
	</pre>
	<p>Simple system timeformat</p>
	<pre class="nospace">
		#request.format("general_timeformat_simple=HHmm")#
	</pre>
	<h4>Admin</h4>
	<p>Role which is needed to access the admin panel</p>
	<pre class="nospace">
		#request.format("admin_accessrole=causer")#
	</pre>
	
	<h4>Library</h4>
	<p>Directory name for CKEditor</p>
	<pre class="nospace">
		#request.format("lib_ckeditor_dir=ckeditor_351")#
	</pre>
	<p>Directory name for ExtJs</p>
	<pre class="nospace">
		#request.format("lib_ext_dir=ext_34")#
	</pre>
	<p>Directory name for Flowplayer</p>
	<pre class="nospace">
		#request.format("lib_flowplayer_dir=flowplayer_323")#
	</pre>
	<p>Directory name for JQuery</p>
	<pre class="nospace">
		#request.format("lib_jquery_dir=jquery_150")#
	</pre>
	<p>Directory name for Plupload</p>
	<pre class="nospace">
		#request.format("lib_plupload_dir=plupload_1432")#
	</pre>
	
	<h4>ExtJs</h4>
	<p>Add for mark a inheritance field</p>
	<pre class="nospace">
		#request.format("extjs_inheritancefield=(e)")#
	</pre>
	<p>Add for mark a required field</p>
	<pre class="nospace">
		#request.format("extjs_requiredfield=(*)")#
	</pre>
	<p>Should the sidebar start in collapsed state in a data tab</p>
	<pre class="nospace">
		#request.format("extjs_sidebar_datatab_collapsed=true")#
	</pre>
	<p>Show loading mask for normal site request</p>
	<pre class="nospace">
		#request.format("extjs_loading_mask=true")#
	</pre>
	<p>Show loading mask for ajax site request</p>
	<pre class="nospace">
		#request.format("extjs_loading_maskuicall=false")#
	</pre>
	<p>Fade out style of the loading mask</p>
	<pre class="nospace">
		#request.format("extjs_loading_style=shift")#
	</pre>
	<p>Default page size for ext stores</p>
	<pre class="nospace">
		#request.format("extjs_page_size=20")#
	</pre>
	<p>Display the accordion header in a detail view</p>
	<pre class="nospace">
		#request.format("extjs_page_detailshowheader=false")#
	</pre>
	<p>Show alert on store load</p>
	<pre class="nospace">
		#request.format("extjs_use_message_load=true")#
	</pre>
	<p>Show alert on store reload</p>
	<pre class="nospace">
		#request.format("extjs_use_message_reload=true")#
	</pre>
	<p>Show alert on store reset</p>
	<pre class="nospace">
		#request.format("extjs_use_message_reset=true")#
	</pre>
	<p>Show alert if the user clicks to fast</p>
	<pre class="nospace">
		#request.format("extjs_use_message_fastclick=true")#
	</pre>
	<p>Property name for json root</p>
	<pre class="nospace">
		#request.format("extjs_property_root=data")#
	</pre>
	<p>Property name for json id</p>
	<pre class="nospace">
		#request.format("extjs_property_id=id")#
	</pre>
	<p>Property name for json uuid</p>
	<pre class="nospace">
		#request.format("extjs_property_uuid=uuid")#
	</pre>
	<p>Property name for json success</p>
	<pre class="nospace">
		#request.format("extjs_property_success=success")#
	</pre>
	<p>Property name for json total</p>
	<pre class="nospace">
		#request.format("extjs_property_total=recordcount")#
	</pre>
	
	<h4>Module</h4>
	<p>Module config file names (Should only be changed by experienced developers)</p>
<pre class="">
#request.format("
module_file_config = config.xml.cfm
module_file_lang = lang.xml.cfm
module_file_role = role.xml.cfm
module_file_setting = setting.xml.cfm
module_file_global = global.cfm
")#
</pre>

	<h4>Ressources</h4>
	<p>Load lokal (client) ressources (Should only be changed by experienced developers)</p>
<pre class="">
#request.format("
ressource_systemAuth = false
ressource_systemError = false
ressource_systemGetUrl = false
ressource_systemInterface = false
ressource_systemJavascript = false
ressource_systemLocale = false
ressource_systemLogdata = false
ressource_systemModule = false
ressource_systemNavigation = false
ressource_systemPostForm = false
ressource_systemRequest = false
")#
</pre>
	
</div>
</cfoutput>