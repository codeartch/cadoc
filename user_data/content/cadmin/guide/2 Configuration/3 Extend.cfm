<cfoutput>
<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">


<div class="alert alert-info">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Remember</h4>
	<ul>
		<li>Initialize the application to see your changes take effect.</li>
		<li>If you make changes in the master instance, all client instances will also be affected.</li>
	</ul>
</div>

<p>The extend.ini file is located in the root of each cadmin instance (master/client).</p>

<h3>Language Keys</h3>

<div id="langPart">
	<p>For each system language there is a own section. In this example there are two languages "de" and "en".</p>
	<p>This code snippet shows the dummy file. Because of this the keys are commented out</p>
<pre>
#request.format("
;---------------------------------------------------------
[de]
;dummykey_deutsch=Ich bin ein deutscher Dummy Key

[en]
;dummykey_english=I am a english dummy key

;---------------------------------------------------------
")#
</pre>
	<p>
	Now we want to change the value of the login username language variable. For this we look at the source code to find out the assigned key name.
	You can also activate the language "keys" as the default one. So the system shows you the keys instead the language variable e.g. {login_username}.
	</p>
	<p>
	In our example the key is named "login_username" so we add the follow entry to the extend.ini file in the "en" section. If we want change the german value too, we set it also in the "de" section.
	</p>
	<pre class="nospace">
		#request.format("login_username=MyCustomUsername")#
	</pre>
	<p>The result should look like:</p>
<pre>
#request.format("
;---------------------------------------------------------
[de]
;dummykey_deutsch=Ich bin ein deutscher Dummy Key

[en]
;dummykey_english=I am a english dummy key
login_username=MyCustomUsername

;---------------------------------------------------------
")#
</pre>



</div>


<h3>Log Keys</h3>
<div id="langPart">
	<p>Log keys are used to encapsulate language strings from the source code, even if the log keys are not in multiple languages.</p>
	<p>For details how to override the value of a log key please look at the "Language Keys" section.</p>
<pre>
#request.format("
;---------------------------------------------------------
[log]
;dummykey_log=Ich bin ein Dummy Logbuch Key

;---------------------------------------------------------
")#
</pre>
</div>


<h3>Error Keys</h3>
<div id="langPart">
	<p>Error keys are used to encapsulate language strings from the source code, even if the error keys are not in multiple languages.</p>
	<p>For details how to override the value of a log key please look at the "Language Keys" section.</p>
<pre>
;---------------------------------------------------------
[error]
;dummykey_error=Ich bin ein Dummy Error Key

;---------------------------------------------------------
</pre>
</div>

</cfoutput>