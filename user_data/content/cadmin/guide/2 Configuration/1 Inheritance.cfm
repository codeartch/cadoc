<cfoutput>
<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<h3>Inheritance</h3>

<p>All system config and language variables gets inherited in the following hierarchy:</p>

<ul>
	<li>Webservice</li>
	<ul>
		<li>Master</li>
		<ul>
			<li>Client 01</li>
			<li>Client 02</li>
			<li>...</li>
		</ul>
	</ul>
</ul>
<p>You can override an inherited value by setting up the key in the current instance (master/client).<br /> If you make a change in the master remember that all assigned clients will be affected too.</p>
</cfoutput>