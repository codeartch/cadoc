<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<cfoutput>

<h3>Module</h3>

<h4>General</h4>
<div id="moduleGeneralPart">
<p>Url informations:</p>
<pre class="">
#request.format("
jsRequestGateway.url.type
jsRequestGateway.url.view
jsRequestGateway.url.entry
jsRequestGateway.url.entrytype
jsRequestGateway.url.parent
jsRequestGateway.url.parenttype
")#
</pre>

<p>Module informations:</p>
<pre class="">
#request.format("
jsRequestGateway.key
jsRequestGateway.modulekey
")#
</pre>

</div>

<h4>Language</h4>
<div id="moduleLangPart">
	<p>Outputs the module lang value in the user language</p>
	<pre class="nospace">
		#request.format('jsRequestGateway.lang["page_tree_delete"]')#
	</pre>
</div>

<h4>Setting</h4>
<div id="moduleSettingPart">
	<p>Outputs the module setting value</p>
	<pre class="nospace">
		#request.format('jsRequestGateway.setting["page_count"]')#
	</pre>
</div>

</cfoutput>
