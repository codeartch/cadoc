<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<cfoutput>
<h3>System</h3>

<h4>General</h4>
<div id="systemGeneralPart">
<p>User informations:</p>
<pre class="">
#request.format("
jsGateway.user.id
jsGateway.user.uuid
jsGateway.user.rights

jsGateway.user.statusnumber
jsGateway.user.issuperadmin
jsGateway.user.isadmin
jsGateway.user.issupermod
jsGateway.user.ismod
jsGateway.user.issuperuser
jsGateway.user.isuser
jsGateway.user.isdev
")#
</pre>
</div>

<p>Url informations:</p>
<pre class="">
#request.format("
jsGateway.url.type
jsGateway.url.view
jsGateway.url.entry
jsGateway.url.entrytype
jsGateway.url.parent
jsGateway.url.parenttype
")#
</pre>

<p>Language informations:</p>
<pre class="">
#request.format("
jsGateway.currentlangid
jsGateway.currentlangcode
")#
</pre>
</div>

<h4>Language</h4>
<div id="systemLangPart">
	<p>Outputs the system lang value in the user language:</p>
	<pre class="nospace">
		#request.format('jsGateway.lang["login_submit"]')#
	</pre>
</div>

<h4>Config</h4>
<div id="systemConfigPart">
	<p>Config: basic part:</p>
	<pre class="nospace">
		#request.format('jsGateway.config.basic["version"]')#
	</pre>
	<p>Config: setting part:</p>
	<pre class="nospace">
		#request.format('jsGateway.config.setting["general_codierung"]')#
	</pre>	
	
	
	<p>Config: http path:</p>
	<pre class="nospace">
		#request.format('jsGateway.config.httppath["module_path"]')#
	</pre>
	<p>Config: http master path:</p>
	<pre class="nospace">
		#request.format('jsGateway.config.masterhttppath["module_path"]')#
	</pre>
</div>

</cfoutput>