<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-info">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Remember</h4>
	<ul>
		<li>Make a file and database backup before you starting with the update process.</li>
	</ul>
</div>

<h3>Update/Copy a client</h3>

<ul>
	<li>1. Copy all custom modules from your old cadmin instance to the new one.</li>
	<li>2. Copy all changed setting.ini and extend.ini entries</li>
	<li>3. Copy all  uploaded files (located in media/upload)</li>
	<li>4. Copy the database</li>
	<li>5. Restart Coldfusion service (On Unix: /etc/init.d/coldfusion restart)</li>
</li>