<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-warning">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Note</h4>
	You must install the master before you can install a client
</div>

<h3>Client Installation</h3>
<p><strong>Database</strong></p>

<ul>
	<li>Create a database.</li>
    <li><strong>Name:</strong> You can choose a own name, i suggest to use the follow syntax "cadmin_client01" use your own project prefix instead of "client01".</li>
    <li><strong>Charset:</strong> utf8</li>
    <li><strong>Collation:</strong> utf8_general_ci</li>
</ul>
<p><strong>Datasource</strong></p>

<ul>
	<li>Go to the Coldfusion Administrator, which is located in "/CFIDE/administrator/index.cfm" on your Coldfusion webserver</li>
	<li>Create a datasource for your new created database. I suggest to accept the same name as your database.</li>
	<li>Enter "allowMultiQueries=true" in the "Advanced Settings" section into the field called "Conection String"</li>
</ul>


<!--- <h3>Useful Server Setting</h3>
<p><strong>Webserver e.g. IIS</strong></p>

<ul>
	<li>Disable CFIDE Access.</li>
	<li>Disable CFC Remote Access.</li>
</ul> --->