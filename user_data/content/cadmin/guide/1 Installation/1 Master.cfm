<cfinclude template="/approot/src/view/object/alert/uptodate.cfm">

<div class="alert alert-warning">
	<a href="##" class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Requirements</h4>
	<p>
		<strong>Requirements</strong><br>
		OS: Linux / OSX / Windows<br />
		Mysql 5.1.x<br />
		Coldfusion 9.0.1.x<br />
	</p>
</div>


<h3>Master Installation</h3>
<p><strong>Coldfusion Administrator</strong></p>
<ul>
	<li><strong>Server Settings &gt; Settings &gt; Use UUID for cftoken</strong> = active</li>

    <li><strong>Server Settings &gt; Settings &gt; Disable CFC Type Check</strong> = deactive</li>
	<li><strong>Server Settings &gt; Settings &gt; Enable In-Memory File System </strong> = active</li>

    <li><strong>Server Settings &gt; Mail &gt; Mail Settings</strong>
		<ul>
			<li>Mail Server: z.B. 192.168.100.1</li>
			<li>User Name: z.B. admin</li>
			<li>Password: z.B. adminpw</li>

		</ul>
	</li>
    <li><strong>Server Settings &gt; Mappings</strong> create a new mapping called "fwroot", pointing to the directory which contains the "cadmin" folder.</li>
	<li><strong>Server Settings &gt; Java and JVM &gt; JVM Arguments</strong>. Append "-Dfile.encoding=UTF-8" (This config is needed for xml parsing in utf-8).</li>

	<li><strong>Server Settings &gt; Java and JVM &gt; JVM Arguments</strong>. Append "-Dcoldfusion.sessioncookie.httponly=true" (This makes JSessionId http only).</li>
	<li>Don't forget to restart the coldfusion service.</li>
</ul>